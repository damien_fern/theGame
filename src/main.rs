use rand::Rng;


fn main() {
    // Générer les cartes
    let mut ascending1 = 1;
    let mut ascending2 = 1;
    let mut descending1 = 100;
    let mut descending2 = 100;

    let mut stack_cards: Vec<i32> = init_stack_cards(descending1);
    let mut hand : Vec<i32> = Vec::with_capacity(8);


    draw(&mut stack_cards, &mut hand, 8);
    let mut index = 0;
    loop {
        let mut min_vec: Vec<i32> = Vec::new();
        let hand_value = hand.get(index).expect("Not found");
        min_vec.push((hand_value - ascending1).abs());
        min_vec.push((hand_value - ascending2).abs());
        min_vec.push((hand_value - descending1).abs());
        min_vec.push((hand_value - descending2).abs());

        let min = min_vec.iter().min().expect("Min value not found");
        println!("{}", min);
        let my_var = min_vec.iter().position(|x| *x == *min).expect("Not Found");
        index += 1;
        println!("{}", my_var);

    }
    // play_card(&mut hand, 4, &mut ascending1);


    println!("hello");
}

fn init_stack_cards(length: i32) -> Vec<i32> {
    let mut cards: Vec<i32> = Vec::with_capacity(length as usize);

    for index in 2..(length) {
        cards.push(index as i32);
    }

    cards
}

fn draw(stack: &mut Vec<i32>, hand: &mut Vec<i32>, cards_played_count: i32) {
    let mut rng = rand::thread_rng();

    for _ in 0..cards_played_count {
        let nb_cards = stack.len();
        let random_index = rng.gen_range(0, nb_cards);
        let random_card = stack.remove(random_index);
        hand.push(random_card);
    }
    hand.sort();
}

fn play_card(hand: &mut Vec<i32>, card_index :i32, pli: &mut i32) {
    *pli = *&hand.remove(card_index as usize);
}

fn count_points(stack: &Vec<i32>, hand: &Vec<i32>) -> i32 {
    return (stack.len() + hand.len()) as i32;
}